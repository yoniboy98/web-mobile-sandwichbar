(function() {
    "use strict";
    /*jslint browser: true*/
    /*jslint devel: true*/
    let apiAddress = "https://myhostingname.be/sandwichshop/";
    /* Vorige lijn aanpassen naar jouw domein! */

    let alertje = document.getElementById("myAlert");
    let opties = {
        method: "POST", // *GET, POST, PUT, DELETE, etc.
        mode: "cors", // no-cors, *cors, same-origin
        cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        credentials: "omit" // include, *same-origin, omit
           
    };

    const FailAlert = "<div class='alert alert-warning alert-dismissible fade show' role='alert'><strong>FAILED!</strong> You should check in on some of those fields below. <button type='button' class='close' data-dismiss='alert' aria-label='Close'> <span aria-hidden='true'>&times;</span> </button></div>";
    const SuccesAlert = "<div class='alert alert-primary alert-dismissible fade show' role='alert'><strong>SUCCES!</strong> Welcome to the club ! <button type='button' class='close' data-dismiss='alert' aria-label='Close'> <span aria-hidden='true'>&times;</span> </button></div>";
    const WarningAlert = "<div class='alert alert-warning alert-dismissible fade show' role='alert'><strong>WARNING!</strong> There is a problem with the server. <button type='button' class='close' data-dismiss='alert' aria-label='Close'> <span aria-hidden='true'>&times;</span> </button></div>";



    function getApiGebruiker() {
		// een ONVEILIGE manier om gebruikersgegevens te testen
    
		let url = apiAddress + "php/api.php?m=login";
		// onze php api verwacht een paar parameters
		// we voegen deze toe aan de body van de opties
    
		// body data type must match "Content-Type" header
		opties.body = JSON.stringify({
			email: document.getElementById("login").value,
			passw: document.getElementById("pass").value,
			format: "json"
		}); 
		
		// test de api
		fetch(url, opties)
			.then(function(response) {
				return response.json();
			})
			.then(function(responseData){
				// test status van de response        
				if(responseData.status < 200 || responseData.status > 299) {
					// login faalde, boodschap weergeven
					// Hier kan je ook een groter onderscheid maken tussen de verschillende vormen van login falen.
					alertMySelf(FailAlert, responseData.error);
					// return, zodat de rest van de fetch niet verder uitgevoerd wordt
					return;
				}
        
				// de verwerking van de data
				var list = responseData.data;
               if(list.role == 1){
				window.location.replace('https://myhostingname.be/sandwichshop/adminMenu.html');
                } else {
					window.location.replace('https://myhostingname.be/sandwichshop/menu.html');
				}
				if (Object.keys(list).length > 0) {
					// list bevat minstens 1 property met waarde
					list.ID = parseInt(list.ID);   
					// alles wat via json komt, is standaard een string of een object.
					// hier is het omzetten naar een int wel niet nodig, omdat we er niet met gaan rekenen
					alertMySelf(SuccesAlert + list.role);
				} else {
					alertMySelf(WarningAlert, responseData.error);
				}

			})
			.catch(function(error) {
				// verwerk de fout
				console.log("fout : " + error);
			});
	}


     function addUser() {
      
     let url = apiAddress + "php/authentication.php?";

    opties.body = JSON.stringify({
         format: "json", 
         bewerking: "add",
         table: "klanten",
         address: document.getElementById("address").value,
         email: document.getElementById("email").value,
         family_name: document.getElementById("familyname").value,
         name: document.getElementById("name").value,      
         passw: document.getElementById("passw").value,
         role: "2",
         telephone_number: document.getElementById("telephonenumber").value,
     });



     fetch(url, opties)
     .then(function(response) {
         return response.json();
     })
     .then(function(responseData) {
         if (responseData.status === "fail") {
            alertMySelf(FailAlert, responseData.error);
         } else {
            alertMySelf(SuccesAlert, responseData.error);
         }
     })
     .catch(function(error) {
         // verwerk de fout
         alertMySelf('POST failed. :' + error, WarningAlert);
     });
 }

 


  document.getElementById("addNewUser").addEventListener("click", function() {
        addUser();
    });
    document.getElementById("loginButton").addEventListener("click", function() {
        getApiGebruiker();
    });


    function alertMySelf(message) {
        alertje.innerHTML = message;
    }


})();