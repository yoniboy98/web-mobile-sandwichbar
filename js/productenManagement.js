(function() {
    "use strict";
    /*jslint browser: true*/
    /*jslint devel: true*/
    let apiAddress = "https://myhostingname.be/sandwichshop/";
    /* Vorige lijn aanpassen naar jouw domein! */
    let alertje = document.getElementById("myAlert");
    let alertEl = document.getElementById("showProducten");
    let opties = {
        method: "POST", // *GET, POST, PUT, DELETE, etc.
        mode: "cors", // no-cors, *cors, same-origin
        cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
        credentials: "omit" // include, *same-origin, omit
           
    };

    const FailAlert = "<div class='alert alert-warning alert-dismissible fade show' role='alert'><strong>FAILED!</strong> You should check in on some of those fields below. <button type='button' class='close' data-dismiss='alert' aria-label='Close'> <span aria-hidden='true'>&times;</span> </button></div>";
    const SuccesAlert = "<div class='alert alert-primary alert-dismissible fade show' role='alert'><strong>SUCCES!</strong> Record is succesfully added ! <button type='button' class='close' data-dismiss='alert' aria-label='Close'> <span aria-hidden='true'>&times;</span> </button></div>";
    const WarningAlert = "<div class='alert alert-warning alert-dismissible fade show' role='alert'><strong>WARNING!</strong> There is a problem with the server. <button type='button' class='close' data-dismiss='alert' aria-label='Close'> <span aria-hidden='true'>&times;</span> </button></div>";


    function getList() {
        // de data opvragen van de andere server (zie les 2)
        let url = apiAddress + "php/producten.php?";

        // body data type must match "Content-Type" header
        opties.body = JSON.stringify({
            format: "json",
            table: "sandwiches",
            bewerking: "get"
        });

        fetch(url, opties)
            .then(function(response) {
                return response.json();
            })
            .then(function(responseData) {
                // de verwerking van de data
                var list = responseData.data;

                if (list.length > 0) {
                    // er zit minstens 1 item in list, we geven dit ook onmiddelijk weer
                    let tableData = ""
                    for (var i = 0; i < list.length; i++) {
                      tableData += "<section class='product_tile'>"+
                     "<div class='section_row'>"+
                         "<div class='section_product_tile row col-10'>"+
                             "<div class='col-4'>"+
                                 "<img src='images/carousel-picture2.jpeg'>"+
                             "</div>"+
                             "<div class='section_meta'>"+
                                 "<p class='product_title'> Name: " + list[i].name + "</p>"+
                                 "<p class='product_info'> Extra: " + list[i].extra + "</p>"+
                                 "<p class='product_info'> kcal: " + list[i].kcal + "</p>"+
                                 "<p class='product_info'>" + list[i].available + "</p>"+
                                 "<p class='product_info'> Sauce: " + list[i].saus + "</p>"+
                                 "<p class='product_price'><i class=''> " + list[i].price + " euro " +
                                 "</span></div></div>"+
                                  "</section>";

                      
                    }
                

                    alerter(tableData);
                   

                }

            })


        return true;
    }









    function deleteProduct(id) {

        let url = apiAddress + "php/producten.php?" ;

        // fetch request opzetten om een item te verwijderen.
        // body data type must match "Content-Type" header
        opties.body = JSON.stringify({
            bewerking: "delete",
            table: "sandwiches",
            id: id,
            format: "json"
        });

        fetch(url, opties)
            .then(function(response) {
                return response.json();
            })
            .then(function(responseData) {
                // de verwerking van de data
                alerter("Die zien we nooit meer ... terug!", "Item verwijderd");

            })
            .catch(function(error) {
                // verwerk de fout
                alerter('POST failed. :' + error, "Item toegevoegd");
            });

    }




    function addProduct() {
        let url = apiAddress + "php/producten.php?";
        // body data type must match "Content-Type" header
        opties.body = JSON.stringify({
            format: "json",
            table: "sandwiches",
            bewerking: "add",
            available: document.getElementById("available").value,
            extra: document.getElementById("extra").value,      
            kcal: document.getElementById("kcal").value,
            name: document.getElementById("name").value,
            price: document.getElementById("price").value,
            saus: document.getElementById("sauce").value,
        
        });
   
        fetch(url, opties)
            .then(function(response) {
                return response.json();
            })
            .then(function(responseData) {
                if (responseData.status === "fail") {
                    alertMySelf(FailAlert, responseData.error);
                } else {
                    alertMySelf(SuccesAlert, responseData.error);
                }
                // refresh de lijst
                getList();
         
            })
            .catch(function(error) {
                // verwerk de fout
                alertMySelf('POST failed. :' + error, WarningAlert);
            });
    }


    function addOrder() {
        let url = apiAddress + "php/producten.php?";
        // body data type must match "Content-Type" header
        opties.body = JSON.stringify({
            format: "json",
            table: "Orders",
            bewerking: "addOrder",
      
    
        });
   
        fetch(url, opties)
            .then(function(response) {
                return response.json();
            })
            .then(function(responseData) {
                if (responseData.status === "fail") {
                    alertMySelf(FailAlert, responseData.error);
                } else {
                    alertMySelf(SuccesAlert, responseData.error);
                }
                // refresh de lijst
                getList();
         
            })
            .catch(function(error) {
                // verwerk de fout
                alertMySelf('POST failed. :' + error, WarningAlert);
            });
    }



    // EventListeners
    document.getElementById("addFoodButton").addEventListener("click", function() {
        addProduct();
    });

    document.getElementById("showProducten").fetch(getList());
  /*  document.getElementById("deleteThisProduct").addEventListener("click", function() {
      deleteProduct();
    });
    */


    // helper functies
    function alerter(message) {
        alertEl.innerHTML = message;
    }
    function alertMySelf(message2) {
        alertje.innerHTML = message2;
    }



})();