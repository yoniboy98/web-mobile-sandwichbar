# Logboek Web & Mobile - Yoni Vindelinckx
domeinnaam: http://www.myhostingname.be


## datum (01/01/2021) - 7uur

- api calls 
- broodjes tonen
- registeren
- inloggen
- broodjes verwijderen
- broodjes toevoegen 

## datum (02/1/2021) - 4uur 

- broodjes bestellen 
- admin en user page 


## datum (03/1/2021) - 1uur 

- broodjes afronden

## datum (04/1/2021) - 8uur 

- css 
- troubleshooting
- api 

## datum (05/1/2021) - 6uur 

- css 
- troubleshooting left join, naam van broodjes tonen 
- hashing wachtwoorden 
- input validation 

## datum (06/1/2021) - 6uur 

- troubleshooting hashing + broodjes afronden.

## datum (07/1/2021) - 5uur 

- comments 
- details
- css




