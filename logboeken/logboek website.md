# Logboek Web & Mobile - Yoni Vindelinckx
domeinnaam: http://www.myhostingname.be/sandwichshop
## datum (25/09/2020) - 1 uur

- Aanmaken van een combell account 
- Domeinnaam opzetten 
- Ftp
- databank

## datum (30/10/2020) - 2uur

- Databank tabellen aanmaken
- Databank koppelen aan php code 
- Tabeldata weergeven op de webpagina

## datum (31/10/2020) - 7uur 

- Volledig responsive design maken
- Werken met bootstrap 4 
- broodjes toevoegen in de databank

## datum (01/11/2020) -  4uur 
- registration troubleshooting

## datum (02/11/2020) -  4uur 
- registration verder troubleshooten + probleem opgelost

## datum (03/11/2020) -  4uur 
- login aanmaken

## datum (04/11/2020) -  7uur 
- extra details en verbeterpunten analyseren en uitwerken.

## datum (05/11/2020) -  3uur 
- Menu list dat wat mooier is voor gebruikers. + troubleshooting regex

## datum (04/12/2020) -  1uur 
- Het toevoegen van broodjes in Orders op php api.
